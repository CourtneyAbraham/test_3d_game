#[allow(dead_code)]
#[derive(Clone, Copy)]
#[repr(u32)]
pub enum CollisionGroup {
    None,
    Visible = 1 << 0,
    Player = 1 << 1,
    All = u32::MAX,
}
