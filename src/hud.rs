use bevy::{input::mouse::MouseWheel, prelude::*};

#[derive(Component)]
pub struct Hud;

#[derive(Component)]
pub struct ProgressBarFill;

pub struct HudPlugin;

impl Plugin for HudPlugin {
    fn build(&self, app: &mut App) {
        app.add_startup_system(setup_system)
            .add_system(change_bar_percent);
    }
}

fn setup_system(mut commands: Commands) {
    commands.spawn_bundle(UiCameraBundle::default());

    commands.spawn_bundle(NodeBundle {
        style: Style {
            size: Size::new(Val::Percent(0.2), Val::Percent(0.2)),
            position_type: PositionType::Absolute,
            position: Rect {
                left: Val::Percent(49.9),
                bottom: Val::Percent(49.9),
                ..default()
            },
            ..default()
        },
        color: Color::WHITE.into(),
        ..default()
    });

    commands
        .spawn_bundle(NodeBundle {
            style: Style {
                size: Size::new(Val::Px(300.0), Val::Px(20.0)),
                position_type: PositionType::Absolute,
                position: Rect {
                    left: Val::Px(10.0),
                    top: Val::Px(10.0),
                    ..default()
                },
                ..default()
            },
            color: Color::WHITE.into(),
            ..default()
        })
        .insert(Hud)
        .with_children(|parent| {
            parent
                .spawn_bundle(NodeBundle {
                    style: Style {
                        size: Size::new(Val::Percent(50.0), Val::Percent(100.0)),
                        position_type: PositionType::Absolute,
                        ..default()
                    },
                    color: Color::RED.into(),
                    ..default()
                })
                .insert(Hud)
                .insert(ProgressBarFill);
        });
}

fn change_bar_percent(
    mut mouse_wheel: EventReader<MouseWheel>,
    mut bar_fill_query: Query<&mut Style, (With<Hud>, With<ProgressBarFill>)>,
) {
    let mut bar_fill = bar_fill_query.single_mut();
    for ev in mouse_wheel.iter() {
        if let Val::Percent(value) = bar_fill.size.width {
            if (value <= 100.0 && ev.y > 0.0) || (value >= 0.0 && ev.y < 0.0) {
                bar_fill.size.width += ev.y;
            }
        }
    }
}
