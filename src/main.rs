use crate::groups::CollisionGroup;
use bevy::{input::system::exit_on_esc_system, prelude::*};
use bevy_rapier3d::prelude::*;

mod groups;
mod hud;
mod player;

fn main() {
    App::new()
        .insert_resource(Msaa { samples: 4 })
        .insert_resource(ClearColor(Color::rgb(0.1, 0.1, 0.1)))
        .insert_resource(WindowDescriptor {
            title: "3D Game".to_string(),
            width: 1920.0,
            height: 1080.0,
            cursor_locked: true,
            cursor_visible: false,
            ..default()
        })
        .add_startup_system(setup_system)
        .add_plugins(DefaultPlugins)
        .add_plugin(RapierPhysicsPlugin::<NoUserData>::default())
        .init_resource::<player::PlayerInfo>()
        .add_plugin(player::PlayerPlugin)
        .add_plugin(hud::HudPlugin)
        .add_system(update_fps)
        .add_system(exit_on_esc_system)
        .run();
}

fn setup_system(
    mut commands: Commands,
    asset_server: Res<AssetServer>,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<StandardMaterial>>,
) {
    commands.spawn_bundle(DirectionalLightBundle {
        transform: Transform::from_rotation(Quat::from_euler(
            EulerRot::XYZ,
            -45.0_f32.to_radians(),
            45.0_f32.to_radians(),
            0.0,
        )),
        directional_light: DirectionalLight {
            illuminance: 32000.0,
            shadows_enabled: true,
            shadow_projection: OrthographicProjection {
                left: -22.0,
                right: 22.0,
                bottom: -22.0,
                top: 22.0,
                near: -10.0 * 22.0,
                far: 10.0 * 22.0,
                ..default()
            },
            ..default()
        },
        ..default()
    });

    commands
        .spawn_bundle(PbrBundle {
            transform: Transform::from_xyz(0.0, 0.0, 0.0),
            mesh: meshes.add(Mesh::from(shape::Plane { size: 25.0 })),
            material: materials.add(Color::rgb(0.7, 0.7, 0.7).into()),
            ..default()
        })
        // Physics components
        .insert(Collider::cuboid(12.5, 0.0, 12.5));

    for x in 0..25 {
        commands
            .spawn_bundle(PbrBundle {
                transform: Transform {
                    translation: Vec3::new(
                        0.0 + (((x % 2) as f32 - 0.5) * 0.1),
                        10.0 + (x as f32 * 1.74),
                        0.0 - (((x % 3) as f32 - 0.5) * 0.1),
                    ),
                    ..default()
                },
                mesh: meshes.add(Mesh::from(shape::Cube { size: 1.0 })),
                material: materials.add(Color::rgb(1.0, 1.0, 1.0).into()),
                ..default()
            })
            .insert(player::Pickup::default())
            .insert(CollisionGroups::new(
                CollisionGroup::Visible as u32,
                CollisionGroup::All as u32,
            ))
            // Physics components
            .insert_bundle((
                RigidBody::Dynamic,
                Collider::cuboid(0.5, 0.5, 0.5),
                Restitution::coefficient(0.7),
                Velocity::default(),
                ColliderMassProperties::Density(2.0),
            ));
    }

    commands
        .spawn_bundle(TransformBundle::from(Transform {
            translation: Vec3::new(-1.0, 2.0, 0.0),
            rotation: Quat::from_rotation_y(-(120.0_f32).to_radians()),
            scale: Vec3::new(0.025, 0.025, 0.025),
        }))
        .with_children(|parent| {
            parent.spawn_scene(asset_server.load("gun.gltf#Scene0"));
        })
        .insert(player::Pickup::default())
        .insert(CollisionGroups::new(
            CollisionGroup::Visible as u32,
            CollisionGroup::All as u32,
        ))
        // Physics components
        .insert_bundle((
            RigidBody::Dynamic,
            Collider::cuboid(1.0, 5.0, 5.0),
            Restitution::coefficient(0.7),
            Velocity::default(),
        ));
}

fn update_fps(time: Res<Time>, mut windows: ResMut<Windows>) {
    let window = windows.get_primary_mut().unwrap();

    let fps = (1.0 / time.delta_seconds()) as u32;

    window.set_title(format!("3D Game - {} FPS", fps));
}
