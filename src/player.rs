use bevy::{input::mouse::MouseMotion, prelude::*};
use bevy_rapier3d::prelude::*;
use smooth_bevy_cameras::{
    controllers::fps::{ControlEvent, FpsCameraBundle, FpsCameraController, FpsCameraPlugin},
    LookAngles, LookTransform, LookTransformPlugin,
};

use crate::groups::CollisionGroup;

const CAMERA_MOVE_SENSITIVTY: f32 = 1.7;
const CAMERA_ROTATE_SENSITIVTY: f32 = 0.5;
const CAMERA_EYE_HEIGHT: f32 = 0.9;

#[derive(Component, Default)]
pub struct Pickup {
    pub in_hand: bool,
}

#[derive(Component)]
pub struct Player;

pub struct PlayerInfo {
    pub spawn_transform: Transform,
    pub movement_speed: f32,
}

impl Default for PlayerInfo {
    fn default() -> Self {
        PlayerInfo {
            movement_speed: 3.0,
            spawn_transform: default(),
        }
    }
}

pub struct PlayerPlugin;

impl Plugin for PlayerPlugin {
    fn build(&self, app: &mut App) {
        app.add_plugin(LookTransformPlugin)
            .add_plugin(FpsCameraPlugin {
                override_input_system: true,
            })
            .add_startup_system(setup_system)
            .add_system(camera_input_system)
            .add_system(movement_system)
            // .add_system(camera_player_lock_system)
            .add_system(pickup_system)
            .add_system(lock_pickup_to_hand_system);
    }
}

fn setup_system(
    mut commands: Commands,
    player_info: Res<PlayerInfo>,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<StandardMaterial>>,
) {
    commands
        .spawn_bundle(PbrBundle {
            transform: Transform::from_xyz(0.0, 0.9, -3.0),
            mesh: meshes.add(Mesh::from(shape::Capsule {
                radius: 0.4,
                depth: 1.0,
                ..default()
            })),
            material: materials.add(Color::rgb(0.7, 0.7, 0.7).into()),
            ..default()
        })
        .insert(Player)
        // Physics components
        .insert_bundle((
            RigidBody::Dynamic,
            ExternalForce::default(),
            Velocity::default(),
            Collider::capsule_y(0.6, 0.4),
            LockedAxes::ROTATION_LOCKED_X
                | LockedAxes::ROTATION_LOCKED_Y
                | LockedAxes::ROTATION_LOCKED_Z,
        ))
        .insert(CollisionGroups::new(
            CollisionGroup::Player as u32,
            CollisionGroup::All as u32,
        ))
        .with_children(|parent| {
            parent
                .spawn_bundle(FpsCameraBundle::new(
                    FpsCameraController {
                        smoothing_weight: 0.0,
                        mouse_rotate_sensitivity: Vec2::splat(CAMERA_ROTATE_SENSITIVTY),
                        translate_sensitivity: CAMERA_MOVE_SENSITIVTY,
                        ..default()
                    },
                    PerspectiveCameraBundle {
                        perspective_projection: PerspectiveProjection {
                            fov: (90.0_f32).to_radians(),
                            ..default()
                        },
                        ..default()
                    },
                    player_info.spawn_transform.translation + Vec3::Y * CAMERA_EYE_HEIGHT,
                    player_info.spawn_transform.translation
                        + player_info.spawn_transform.forward()
                        + Vec3::Y * CAMERA_EYE_HEIGHT,
                ))
                .insert(Player);
        });
}

fn movement_system(
    keyboard: Res<Input<KeyCode>>,
    player_info: Res<PlayerInfo>,
    mut player: Query<&mut Velocity, With<Player>>,
    camera: Query<&LookTransform, (With<Camera>, With<Player>)>,
) {
    let mut movement = Vec3::ZERO;
    let camera_transform = camera.single();

    for (key, dir) in [
        (KeyCode::W, Vec3::Z),
        (KeyCode::A, Vec3::X),
        (KeyCode::S, -Vec3::Z),
        (KeyCode::D, -Vec3::X),
    ]
    .into_iter()
    {
        if keyboard.pressed(key) {
            let look_vector = camera_transform.look_direction().unwrap();
            let look_angles = LookAngles::from_vector(look_vector);

            let yaw_rot = Quat::from_axis_angle(Vec3::Y, look_angles.get_yaw());
            let rot_x = yaw_rot * Vec3::X;
            let rot_y = yaw_rot * Vec3::Y;
            let rot_z = yaw_rot * Vec3::Z;

            movement += dir.x * rot_x + dir.y * rot_y + dir.z * rot_z;
            movement = movement.normalize();
        }
    }

    let mut velocity = player.single_mut();
    velocity.linvel.x = movement.x * player_info.movement_speed;
    velocity.linvel.z = movement.z * player_info.movement_speed;
}

fn camera_input_system(
    mut events: EventWriter<ControlEvent>,
    time: Res<Time>,
    mut mouse_motion_events: EventReader<MouseMotion>,
    controllers: Query<&FpsCameraController>,
) {
    // Can only control one camera at a time.
    let controller = if let Some(controller) = controllers.iter().find(|c| c.enabled) {
        controller
    } else {
        return;
    };
    let mouse_rotate_sensitivity = controller.mouse_rotate_sensitivity;

    let mut cursor_delta = Vec2::ZERO;
    for event in mouse_motion_events.iter() {
        cursor_delta += event.delta;
    }

    events.send(ControlEvent::Rotate(
        mouse_rotate_sensitivity * cursor_delta * time.delta_seconds(),
    ));
}

fn pickup_system(
    mouse_clicks: Res<Input<MouseButton>>,
    rapier_context: Res<RapierContext>,
    player: Query<(&Transform, &Children), With<Player>>,
    camera_tf_query: Query<&LookTransform, With<Player>>,
    mut pickups: Query<(Entity, &mut Pickup, &mut CollisionGroups)>,
) {
    if mouse_clicks.just_pressed(MouseButton::Left) {
        // let camera_transform = camera_transform.single();
        let mut camera_tf = &LookTransform {
            eye: Vec3::ZERO,
            target: Vec3::ZERO,
        };

        let (player_tf, player_children) = player.single();

        for child in player_children.iter() {
            camera_tf = camera_tf_query.get(*child).unwrap();
        }

        if let Some((entity, _)) = rapier_context.cast_ray(
            player_tf.translation + camera_tf.eye,
            camera_tf.look_direction().unwrap(),
            4.0,
            true,
            InteractionGroups::new(CollisionGroup::All as u32, !(CollisionGroup::Player as u32)),
            None,
        ) {
            for (pickup_entity, mut pickup, mut collision_groups) in pickups.iter_mut() {
                if entity == pickup_entity {
                    pickup.in_hand = true;
                }
                collision_groups.filters &= !(CollisionGroup::Player as u32);
            }
        }
    }

    if mouse_clicks.just_released(MouseButton::Left) {
        for (_, mut pickup, mut collision_groups) in pickups.iter_mut() {
            if pickup.in_hand == true {
                pickup.in_hand = false;
            }
            collision_groups.filters |= CollisionGroup::Player as u32;
        }
    }
}

fn lock_pickup_to_hand_system(
    player: Query<(&Transform, &Children), With<Player>>,
    camera_tf_query: Query<&LookTransform, With<Player>>,
    mut pickups: Query<(&Transform, &mut Velocity, &Pickup)>,
) {
    let mut camera_tf = &LookTransform {
        eye: Vec3::ZERO,
        target: Vec3::ZERO,
    };

    let (player_tf, player_children) = player.single();

    for child in player_children.iter() {
        camera_tf = camera_tf_query.get(*child).unwrap();
    }

    for (pickup_tf, mut pickup_velocity, pickup) in pickups.iter_mut() {
        if pickup.in_hand == false {
            continue;
        }

        pickup_velocity.linvel = 10.0
            * ((player_tf.translation + camera_tf.eye + camera_tf.look_direction().unwrap() * 2.0)
                - pickup_tf.translation);
    }
}
